 class Tweet < ActiveRecord::Base  #class "Tweet" inherits ActiveRecord::Base
     belongs_to :zombie              # sets relationship of tweet table to zombie table
    validates_presence_of :status #validates that "status" is never blank.
end
